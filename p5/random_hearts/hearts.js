let h = 800;
let w = 800;

function setup() {
  cvs = createCanvas(h, w);
  // colorMode(HSB);
  colorMode(RGB, 255);
  smooth();
  noStroke();
  noLoop();
}

let number = 100000;
// colors from: https://www.colourlovers.com/palette/1996450/Happy_Hearts
let colors = ["#FF84C7", "#FA0F93", "#E50440", "#CE004B"];

function draw() {
  background(256);
  for (let i = 0; i < number; i++) {
    size = random(10, 50)
    x = random(w)/2;
    y = random(h)/2;
    a = random(359);
    translate(x, y);
    rotate(a);
    fill(color(random(colors)));
    heart(x, y, size);
  }
}

function heart(x, y, size) {
  beginShape();
  vertex(x, y);
  bezierVertex(x - size / 2, y - size / 2, x - size, y + size / 3, x, y + size);
  bezierVertex(x + size, y + size / 3, x + size / 2, y - size / 2, x, y);
  endShape(CLOSE);
}

function keyTyped() {
  if (key === 's') {
    saveCanvas(cvs, 'hearts', 'png');
  }
}
